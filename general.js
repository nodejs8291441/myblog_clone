const general = {
  title: "My Blog with EJS",
  header: {
    phone: "986-71-51-37-79",
    mail: "infinity",
    logo: "https://infinitythemes.ge/aden-examples/wp-content/uploads/2015/12/logo-111.png",
    background:
      "https://infinitythemes.ge/aden-examples/wp-content/uploads/2016/01/header-background.jpg",
  },
  footer: {
    image1: "https://infinitythemes.ge/images/instagram/9.jpg",
    image2: "https://infinitythemes.ge/images/instagram/8.jpg",
    image3: "https://infinitythemes.ge/images/instagram/7.jpg",
    image4: "https://infinitythemes.ge/images/instagram/6.jpg",
    image5: "https://infinitythemes.ge/images/instagram/5.jpg",
    image6: "https://infinitythemes.ge/images/instagram/4.jpg",
    image7: "https://infinitythemes.ge/images/instagram/3.jpg",
    image8: "https://infinitythemes.ge/images/instagram/2.jpg",
    insta_title: "@follow us",
    facebook: "facebook",
    twitter: "twitter",
    linkedin: "linkedin",
    bloglovin: "bloglovin",
    tumblr: "tumblr",
    pinterest: "pinterest",
    instagram: "instagram",
    footer_logo:
      "https://infinitythemes.ge/aden-examples/home-v3/wp-content/uploads/sites/13/2016/01/FOOTER-logo.png",
    copyright: "Copyrights © 2016 InfinityThemes. All Rights Reserved."
    },
};

export default general 