import express, { json } from "express";
import Post from "../models/Post.js";
import User from "../models/User.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import general from "../../general.js"
import multer from "multer";
import sharp from "sharp";
import fs from "fs"
import path from "path";
const routes = express.Router();
const jwtSecret = "MySecretBlog";

const authMiddleware = (req, res, next) => {
  const token = req.cookies.token;
  if (!token) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  try {
    const decoded = jwt.verify(token, jwtSecret);
    req.userId = decoded.userId;
    next();
  } catch (error) {
    return res.status(401).json({ message: "Unauthorized" });
  }
};
const storage = multer.memoryStorage()

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 25 * 1024 * 1024, // 25MB
  },
});

routes.get("/", async (req, res) => {
  try {
    // const data = await Post.find();
    let perPage = 6;
    let page = req.query.page || 1;
    const locals = general
    const data = await Post.aggregate([{ $sort: { date: -1 } }])
      .skip(perPage * page - perPage)
      .limit(perPage)
      .exec();
    const count = await Post.count();
    const nextPage = parseInt(page) + 1;
    const hasNextPage = nextPage <= Math.ceil(count / perPage);
    const toSeoUrl = (title) => {
      return title
        .toLowerCase()
        .replace(/[^\w\s]/gi, "") // Loại bỏ các ký tự đặc biệt
        .replace(/\s+/g, "-") // Thay thế khoảng trắng bằng dấu gạch nối
        .substring(0, 50); // Giới hạn độ dài URL (nếu cần)
    };
    const dataSwiper = await Post.find().limit(4)
    res.render("index", {
      layout: "layouts/main",
      locals,
      data,
      current: page,
      nextPage: hasNextPage ? nextPage : null,
      toSeoUrl,
      dataSwiper
    });
  } catch (error) {
    console.log(error);
  }
});

routes.get("/post/:id/:seoTitle", async (req, res) => {
  try {
    let slug = req.params.id;
    const data = await Post.findById({ _id: slug });
    const locals = {
      title: data.title,
    };
    res.render("post", { layout: "layouts/main", locals, data });
  } catch (error) {
    console.log(error);
  }
});

routes.post("/search", async (req, res) => {
  try {
    const locals = general;
    let searchTerm = req.body.searchTerm;
    const searchNoSpecialChar = searchTerm.replace(/[^a-zA-Z0-9]/g, "");
    const data = await Post.find({
      $or: [
        { title: { $regex: new RegExp(searchNoSpecialChar, "i") } },
        { body: { $regex: new RegExp(searchNoSpecialChar, "i") } },
        { category: { $regex: new RegExp(searchNoSpecialChar, "i") } },
      ],
    });
    res.render("search", { data, locals, layout: "layouts/main" });
    // res.render("index", { layout: "layouts/main", locals, data });
  } catch (error) {
    console.log(error);
  }
});

routes.get("/admin", async (req, res) => {
  try {
    res.render("login", { layout: "layouts/login.ejs" });
  } catch (error) {
    console.log(error);
  }
});

routes.post("/admin", async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(401).json({
        message: "Invalid credentials",
      });
    }
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      return res.status(401).json({ message: "Invalid Credentials" });
    }
    const token = jwt.sign({ userId: user._id }, jwtSecret);
    res.cookie("token", token, { httpOnly: true });
    res.redirect("/dashboard");
  } catch (error) {
    console.log(error);
  }
});

routes.get("/dashboard", authMiddleware, async (req, res) => {
  try {
    const data = await Post.find();
    res.render("dashboard", { layout: "layouts/admin", data });
  } catch (error) {
    console.log(error);
  }
});

routes.get("/admin/edit/:id", authMiddleware, async (req, res) => {
  try {
    const data = await Post.findOne({ _id: req.params.id });
    const date = data.date;
    const formattedDate = `${date.getFullYear()}-${String(
      date.getMonth() + 1
    ).padStart(2, "0")}-${String(date.getDate()).padStart(2, "0")}`;
    res.render("editPost", { layout: "layouts/admin", data, formattedDate });
  } catch (error) {
    console.log(error);
  }
});

routes.put("/admin/edit/:id", authMiddleware, async (req, res) => {
  try {
    const { title, category, img, body, date } = req.body;
    if (!date) {
      return res.status(400).send("Date is required");
    }
    const updatedPost = await Post.findByIdAndUpdate(
      req.params.id,
      {
        title,
        category,
        img,
        body,
        date: new Date(date), // Convert the date string to a JavaScript Date object
      },
      { new: true }
    ); // { new: true } returns the updated document
    if (!updatedPost) {
      return res.status(404).send("Post not found");
    }
    res.redirect(`/admin/edit/${req.params.id}`);
  } catch (error) {
    console.log(error);
    res.status(500).send("Server Error");
  }
});

routes.get("/admin/add", authMiddleware, (req, res) => {
  res.render("addPost", { layout: "layouts/admin" });
});

routes.post("/admin/add", authMiddleware, async (req, res) => {
  try {
    console.log(req.body);
    try {
      const newPost = new Post({
        title: req.body.title,
        category: req.body.category,
        img: req.body.img,
        body: req.body.body,
        date: req.body.date,
      });
      await Post.create(newPost)
      res.json({ success: true, message: "Data saved successfully!" });
    } catch (error) {
      console.log(error);
    }
  } catch (error) {
    console.log(error);
    res.json({ success: false, message: "Error saving data." });
  }
});

routes.get("/register", async (req, res) => {
  try {
    res.render("register", { layout: "layouts/login.ejs" });
  } catch (error) {
    console.log(error);
  }
});

routes.post("/register", async (req, res) => {
  try {
    const { username, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, 10);
    try {
      const user = await User.create({ username, password: hashedPassword });
      res.status(201).json({ message: "User created", user });
    } catch (error) {
      if (error.code === 11000) {
        res.status(409).json({ message: "User already in use" });
      }
      res.status(500).json({ message: "Internal server error" });
    }
  } catch (error) {
    console.log(error);
  }
});

routes.post("/admin/delete/:id", async (req, res) => {
  try {
    const postId = req.params.id;
    const result = await Post.findByIdAndRemove(postId);

    if (!result) {
      return res
        .status(404)
        .json({ success: false, message: "Post not found" });
    }

    res.json({ success: true, message: "Post deleted successfully" });
  } catch (error) {
    console.error("Error deleting post:", error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

routes.get("/logout", (req, res) => {
  res.clearCookie("token");
  res.json({ message: "Logout Successful" });
  res.redirect("/admin");
});
routes.post("/upload", upload.single("image"),async (req, res) => {
 try {
  const resizedImageBuffer = await sharp(req.file.buffer)
  .resize(Number(req.body.width), Number(req.body.height))
  .toBuffer()
  res.writeHead(200, {
    "Content-Type": "image/png",
    "Content-Disposition": 'attachment; filename ="resized_image.png'
  })
  res.end(resizedImageBuffer)
 } catch(error) {
  if (error instanceof multer.MulterError && error.code === "LIMIT_FILE_SIZE") {
    return res.status(400).send("Kích thước ảnh không được vượt quá 25MB");
  }
  next(error);
 }

});
routes.get('/upload', (req,res) => {
  res.render('upload' , {layout: 'layouts/admin'})
})
export default routes;

// function insertPostData() {
//   Post.insertMany([
//     {
//       title: "Start Day Positive",
//       category: "Travel",
//       img: "https://infinitythemes.ge/aden-examples/wp-content/uploads/2015/12/Untitled-23-420x280.jpg",
//       body:
//         " Nastidia eloquentiam eailud dolorem nec. Easividit solum equidem. Vix at aquod onium fuisset. Pute ornatus asueverit hiseat, quodlaudel. Mxcrecusa bore tarue simil maque, velit elitr, est primis labitur Donec vitae libero manid commune no definitionem…",
//       date: "2023-10-27T10:30:00.000Z"
//     },
//     {
//         title: "Find Your Place",
//         category: "Nature",
//         img: "https://infinitythemes.ge/aden-examples/wp-content/uploads/2015/12/Untitled-26-420x280.jpg",
//         body: "Rastidia eloquentiam eailud dolorem nec. Easividit solum equidem. Vix at aquod onium fuisset. Pute ornatus asueverit hiseat, quod laudel. Mxcrecusa bore tarue simil maque, velit elitr, est primis labitur Donec vitae libero manid commune no definitionem…",
//         date: "2023-10-27T10:30:00.000Z"
//       },
//       {
//         title: "Woman On The Street",
//         category: "Creative",
//         img: "https://infinitythemes.ge/aden-examples/wp-content/uploads/2015/09/young-690958_1280.jpg",
//         body: "Fastidia eloquentiam eailud dolorem nec. Easmdaet solum equidem. Vix at aquod onium fuisset. uramt ornatus asueverit hiseat, malona swaer kanebimra laudel. Mxcrecusa bore tarue simil maque, velit elit primse labitur Donec vitae libero manid commune viscua…",
//         date: "2023-10-27T10:30:00.000Z"
//       },
//       {
//         title: "Choose Trendy Clothes",
//         category: "LifeStyle",
//         img: "https://infinitythemes.ge/aden-examples/wp-content/uploads/2015/04/15427793078_6def5ec51c_o-420x280.jpg",
//         body: "Sastidia eloquentiam eailud dolorem nec. Easmdaet solum equidem. Vix at aquod onium fuisset. uramt ornatus asueverit hiseat, malona swaer kanebimra laudel. Mxcrecusa bore tarue simil maque, velit elit primse labitur Donec vitae libero manid commune viscua…",
//         date: "2023-10-27T10:30:00.000Z"
//       },
//       {
//         title: "Coldplay – Magic",
//         category: "Creative",
//         img: "https://infinitythemes.ge/aden-examples/wp-content/uploads/2015/03/de9uL9L7RSmzV4SAoAO5_Lauren-and-Winona-Under-a-pass-11-420x280.jpg",
//         body: "Dastidia eloquentiam eailud doloreca.Easa solurm equidem. Auod onium fuisset. Pat oratus suevrit hiseat, laudel. Mxcrecusa bore tarue primis velia elitr, est primis labitur Donec vitae libero manid commune no definit mone vis mode ratius dilam cont….",
//         date: "2023-10-27T10:30:00.000Z"
//       },
//       {
//         title: "New Photo Gallery",
//         category: "Nature",
//         img: "https://infinitythemes.ge/aden-examples/wp-content/uploads/2015/03/Preview-Image-04-420x280.jpg",
//         body: "Kastidia eloquentiam eailud dolorem nec. Easividit solum equidem. Vix at aquod onium fuisset. Pute ornatus asueverit hiseat, quod laudel. Mxcrecusa bore tarue simil maque, velit elitr, est primis labitur Donec vitae libero manid commune no definitionem…",
//         date: "2023-10-27T10:30:00.000Z"
//       },
//   ]);
// }
// insertPostData()
