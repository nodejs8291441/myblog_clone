import express from "express";
import routes from "./server/routes/routes.js";
import expressEjsLayouts from "express-ejs-layouts";
import connectDB from './server/config/db.js'
const app = express();
const port = process.env.port || 8083
import cookieParser from "cookie-parser";
import MongoStore from "connect-mongo";
import session from "express-session";
import methodOverride from "method-override";

// setup for static files 
app.use(express.static('public'))

connectDB()
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(cookieParser())
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    store: MongoStore.create({
        mongoUrl: process.env.MONGODB_URI
    })
}))
app.use(methodOverride('_method'))
  
//ejs setup
app.use(expressEjsLayouts)  
app.set('layout', './layouts/main')

app.set('view engine' , 'ejs');

//create routes
app.use(routes);

app.listen(port, () =>{
    console.log(`server is running on port ${port}`);
})
